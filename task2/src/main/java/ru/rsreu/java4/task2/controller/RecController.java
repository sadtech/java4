package ru.rsreu.java4.task2.controller;

import ru.rsreu.java4.task2.board.Core;
import ru.rsreu.java4.task2.presentation.Presentation;

import java.util.Scanner;

public class RecController implements Controller {

    private Core core;
    private Presentation presentation;

    public RecController(Core core, Presentation presentation) {
        this.core = core;
        this.presentation = presentation;
    }

    @Override
    public void run() {
        core.work();
        presentation.show();
        System.out.print("Prodoljit vvod: ");
        Scanner scanner = new Scanner(System.in);
        while (scanner.nextLine().equals("y")) {
            core.work();
            presentation.show();
            System.out.print("Prodoljit vvod: ");
        }
    }
}
